package com.craftsteamg.jarmodifier;

import org.apache.commons.io.IOUtils;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.tree.ClassNode;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class JarModifier {


    public static void main(String[] args) throws IOException {

        String originalJarLocation = args[0];
        String uuid = args[1];
        int marker1 = Integer.parseInt(args[2]);
        int marker2 = Integer.parseInt(args[3]);
        String output = args[4];


        File file = new File(originalJarLocation);
        if (!file.exists())
            throw new RuntimeException("Jar not found!");


        Map<String, ClassNode> classes = new HashMap<>();
        JarFile jar = new JarFile(file);
        jar.stream().forEach(e -> readJar(jar, e, classes, uuid, marker1, marker2));
        jar.close();

        Map<String, byte[]> out = processNodes(classes);
        Map<String, byte[]> nonClassEntries = loadNonClasses(file);
        out.putAll(nonClassEntries);

        saveAsJar(out, output);
    }

    static Map<String, byte[]> processNodes(Map<String, ClassNode> nodes) {
        Map<String, byte[]> out = new HashMap<>();
        for (ClassNode cn : nodes.values()) {
            ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_MAXS);
            cn.accept(cw);
            out.put(cn.name, cw.toByteArray());
        }
        return out;
    }

    static Map<String, byte[]> loadNonClasses(File jarFile) throws IOException {
        Map<String, byte[]> entries = new HashMap<>();
        ZipInputStream jis = new ZipInputStream(new FileInputStream(jarFile));
        ZipEntry entry;
        while ((entry = jis.getNextEntry()) != null) {
            try {
                String name = entry.getName();
                if (!name.endsWith(".class") && !entry.isDirectory()) {
                    byte[] bytes = IOUtils.toByteArray(jis);
                    entries.put(name, bytes);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                jis.closeEntry();
            }
        }
        jis.close();
        return entries;
    }

    static void saveAsJar(Map<String, byte[]> outBytes, String fileName) {
        try {
            JarOutputStream out = new JarOutputStream(new FileOutputStream(fileName));
            for (String entry : outBytes.keySet()) {
                String ext = entry.contains(".") ? "" : ".class";
                out.putNextEntry(new ZipEntry(entry + ext));
                out.write(outBytes.get(entry));
                out.closeEntry();
            }
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static Map<String, ClassNode> readJar(JarFile jar, JarEntry entry, Map<String, ClassNode> classes, String uuid, int marker1, int marker2) {
        String name = entry.getName();
        try (InputStream jis = jar.getInputStream(entry)) {
            if (name.endsWith(".class")) {
                byte[] bytes = IOUtils.toByteArray(jis);
                try {

                    ClassNode cn = new ClassNode();
                    ConvertingClassVisitor cv = new ConvertingClassVisitor(uuid, marker1, marker2, cn);
                    ClassReader reader = new ClassReader(bytes);
                    reader.accept(cv, 0);

                    classes.put(cn.name, cn);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return classes;
    }



}
