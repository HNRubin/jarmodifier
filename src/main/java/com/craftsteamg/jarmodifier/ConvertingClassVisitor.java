package com.craftsteamg.jarmodifier;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.Type;

import java.util.List;

import static org.objectweb.asm.Opcodes.*;

public class ConvertingClassVisitor extends ClassVisitor {

    private final ClassVisitor classVisitor;
    private final String UUID;
    private final int marker1;
    private final int marker2;

    public ConvertingClassVisitor(String UUID, int marker1, int marker2, ClassVisitor classVisitor) {
        super(ASM4, classVisitor);
        this.UUID = UUID;
        this.marker1 = marker1;
        this.marker2 = marker2;
        this.classVisitor = classVisitor;
    }

    @Override
    public FieldVisitor visitField(int access, String name, String descriptor, String signature, Object value) {

        switch (name) {
            case "marker1":
                return classVisitor.visitField(access | ACC_STATIC, "x1", descriptor, "I", marker1);
            case "marker2":
                return classVisitor.visitField(access | ACC_STATIC, "x2", descriptor, "I", marker2);
            case "apolloKey":
                return classVisitor.visitField(access | ACC_STATIC, name, descriptor, Type.getType(String.class).getDescriptor(), UUID);
            default:
                return classVisitor.visitField(access, name, descriptor, signature, value);

        }

    }


}
